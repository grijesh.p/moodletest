<?php


use core_external\util as external_util;


class block_moodletest extends block_base {
    function init() {
        $this->title = get_string('pluginname', 'block_moodletest');
    }

    function get_content() {
       
        global $DB;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;

        $course = $this->page->course;

        require_once($CFG->dirroot.'/course/lib.php');

        $modinfo = get_fast_modinfo($course);

        /**
         * getting course modules
         */
        foreach($modinfo->get_cms() as $cmid => $cm) {

            $completion_status = $DB->get_record('course_modules_completion', ['coursemoduleid' => $cmid]);

            $status = '';
            if($completion_status == 1){
                $status = 'completed';
            }
            $time_created = strtotime(date('d-M-Y', $cm->timecreated));
            if ($cm->modname === 'resources') {
                $this->content->items[] = '<a href="'.$CFG->wwwroot.'/course/resources.php?id='.$course->id.'">'.$cm->name.' '.$time_created.' '.$status.'</a>';
            } else {
                $this->content->items[] = '<a href="'.$CFG->wwwroot.'/mod/'.$cm->modname.'/index.php?id='.$course->id.'">'.$cm->name.' '.$time_created.' '.$status.'</a>';
            }
        }

        return $this->content;
    }

}